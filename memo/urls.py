from .import views
from django.urls import path

app_name = 'memo'
urlpatterns=[
    path('',views.IndexView.as_view(),name='index'),
    path('inquiry/',views.InquiryView.as_view(),name='inquiry'),
    path('memo-list/',views.MemoListView.as_view(),name='memo_list'),
    path('memo-list/<str:cote>/>',views.MemoListView.as_view(),name='memo_list_c'),
    path('memo-detail/<int:pk>/', views.MemoDetailView.as_view(), name='memo_detail'),
    path('memo-create/', views.MemoCreateView.as_view(), name='memo_create'),
    path('memo-update/<int:pk>/', views.MemoUpdateView.as_view(), name='memo_update'),
    path('memo-delete/<int:pk>/', views.MemoDeleteView.as_view(), name='memo_delete'),
    path('redirect/<int:pk>/', views.Swichbyflag.as_view(), name='redirect'),
]